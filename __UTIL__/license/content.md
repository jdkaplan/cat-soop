<python>
# This file is part of CAT-SOOP
# Copyright (c) 2011-2017 Adam Hartz <hartz@mit.edu>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the Soopycat License, version 2.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the Soopycat License for more details.

# You should have received a copy of the Soopycat License along with this
# program.  If not, see <https://smatz.net/soopycat>.

cs_content_header = "SOOPYCAT LICENSE"
cs_title = "Soopycat License"
</python> 

Version 2
<br/>Copyright &copy; 2016 Adam Hartz &lt;`hartz@mit.edu`&gt;
<br/>Everyone is permitted to copy and distribute verbatim copies of this license
document, and changing it is allowed as long as the name is changed.

## TERMS AND CONDITIONS

Using, modifying, and copying the software licensed under this license is
permitted, provided that the following conditions are met:

1. Copies of source code (in whole or in part, with or without modification)
    must retain all relevant copyright notices, this list of conditions and the
    following disclaimer.

2. Copies in all other forms (in whole or in part, with or without
    modification) must reproduce all relevant copyright notices, this list of
    conditions and the following disclaimer in the documentation and/or other
    materials included with the copy.

3. Copies (in any form, in whole or in part, with or without modification) must
    prominently offer all users receiving them or interacting with them
    (including remotely through a computer network) information on how to
    obtain complete corresponding machine-readable source code for the copy and
    any software that uses the copy, including plugins designed to interact with
    the copy through a documented plugin interface but excluding source files
    intended as input for the resulting software.

    The source code must either be included with the copy or made available
    from a network server at no additional charge, through some standard or
    customary means of faciliating copying of software.

    The source code must be provided in the preferred form for making
    modifications to it and must be licensed in its entirety at no charge to
    all third parties, either:

    * under the terms of this license (either version 2 or, at your option,
        any later version); or

    * under the terms of the GNU Affero General Public License as published
        by the Free Software Foundation (either version 3 or, at your option,
        any later version).

THE SOFTWARE LICENSED UNDER THIS LICENSE IS PROVIDED "AS IS", WITHOUT WARRANTY
OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
