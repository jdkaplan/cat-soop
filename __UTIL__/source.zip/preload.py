SOURCE_README = '''This archive contains the source code for the CAT-SOOP instance at %s, automatically generated on %s.

The (potentially modified) source of the base system is located in the "cat-soop" directory.

If the course in question (%s) used any plugins (including custom question types or content handlers), these are located in the "%s" directory.

CAT-SOOP
https://cat-soop.org'''

SOURCE_README_NOCOURSE = '''This archive contains the source code for the CAT-SOOP instance at %s, automatically generated on %s.

The (potentially modified) source of the base system is located in the "cat-soop" directory.

CAT-SOOP
https://cat-soop.org'''

